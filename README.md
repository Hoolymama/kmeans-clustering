

This script requires python 3.x

Go to a terminal and type:

python3 --version

You should see:

Python 3.7.7 or similar

If not, install Python3

-----

You'll need the following packages:

matplotlib
numpy
opencv-contrib-python
scikit-learn

you can install them like so:

pip3 install matplotlib numpy opencv-contrib-python scikit-learn

Then cd into the kmeans-clustering folder and type commands:

# Use median filtering - 8 clusters (default) - save results to output folder my_small_images
python3  kmclusters.py -o my_images -m 9 images/clubuk_25pc.jpg

# Use bilateral filtering - 16 clusters - save results to output folder my_large_images
python3  kmclusters.py -o my_large_images -b 9 75 75 -c 16 images/clubuk.jpg

USAGE:

positional arguments:
  image                 Path to the image

optional arguments:
  -h, --help            show this help message and exit
  -o OUTDIR, --outdir OUTDIR
                        Output folder: defaults to current folder
  -m MEDIAN, --median MEDIAN
                        Use median filter with kernel size in pixels. It
                        should be a positive odd number
  -b BILATERAL BILATERAL BILATERAL, --bilateral BILATERAL BILATERAL BILATERAL
                        Bilateral size - 3 numbers are (size, sigmaColor,
                        sigmaSpace) - see help https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html#bilateralfilter
  -c CLUSTERS, --clusters CLUSTERS
                        # of clusters (default: 8)
  -s {lab,yrb,hsv,rgb}, --space {lab,yrb,hsv,rgb}
                        Color Space for distance computation. (default: yrb)