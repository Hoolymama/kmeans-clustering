# import the necessary packages
import numpy as np
import cv2
import os
import numpy as np
from sklearn.cluster import KMeans

SPACE_MAP = {
    "lab": {
        "from_bgr": cv2.COLOR_BGR2LAB,
        "to_bgr": cv2.COLOR_LAB2BGR,
    },
    "hsv": {
        "from_bgr": cv2.COLOR_BGR2HSV,
        "to_bgr": cv2.COLOR_HSV2BGR,
    },
    "yrb": {
        "from_bgr": cv2.COLOR_BGR2YCrCb,
        "to_bgr": cv2.COLOR_YCrCb2BGR,
    },
    "rgb": {
        "from_bgr": cv2.COLOR_BGR2RGB,
        "to_bgr": cv2.COLOR_RGB2BGR,
    }
}


def centroid_histogram(labels):
    # create a histogram based on the pixels count of each cluster
    numLabels = np.arange(0, len(np.unique(labels)) + 1)
    (hist, _) = np.histogram(labels, bins=numLabels)
    # normalize
    hist = hist.astype("float")
    hist /= hist.sum()
    return hist


def plot_colors(hist, palette):
    # initialize bar chart
    bar = np.zeros((50, 300, 3), dtype="uint8")
    startX = 0
    for (percent, color) in zip(hist, palette):
        # plot the relative percentage of each cluster
        endX = startX + (percent * 300)
        cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
                      color.astype("uint8").tolist(), -1)
        startX = endX
    return bar


def outline(index_img):
    # make a white outline on black image.
    # Invert later.

    # grab width and height
    (h, w) = index_img.shape[:2]

    # allocate memory for the outline image
    output = np.zeros((h, w), dtype="uint8")

    # loop pixels in 2D
    for y in np.arange(h):
        for x in np.arange(w):

            # define the kernel extents around the current pixel.
            # If the pixel is at the image border then there will be fewer
            # adjacent pixels.
            starty = 0 if y == 0 else -1
            endy = 1 if y == h-1 else 2
            startx = 0 if x == 0 else -1
            endx = 1 if x == w-1 else 2

            # loop over the above kernel around each pixel
            # and if any pixel is different, then we are on the border,
            # so draw a white pixel to contribute to the outline.
            edge = False
            for ky in range(starty+y, endy+y):
                for kx in range(startx+x, endx+x):
                    if index_img.item(ky, kx) != index_img.item(y, x):
                        edge = True
                    if edge:
                        break
                if edge:
                    break
            if edge:
                output.itemset((y, x), 255)
    return output


def ensure_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def filter_image(image, args):

    if args["bilateral"]:
        bilateral =[int(v) for v in  args["bilateral"]]
        if bilateral[0]:
            image = cv2.bilateralFilter(image,bilateral[0],bilateral[1],bilateral[2])
            print("Run bilateral filter: size={}, sigmacolor={}, sigmaspace={}".format(*args["bilateral"]))


    elif args["median"]:
        median = int(args["median"])
        if median:
            image = cv2.medianBlur(image,median)
            print("Run median filter: size={} ".format(args["median"]))
    
    return image


def get_palette(palette_image, space,  clusters):
    """Use Kmeans clustering to make a palette."""
    palette_image = os.path.abspath(palette_image)
    prefix_path =  os.path.splitext(palette_image)[0]
    palette_filename =  "{}_{}_{}.txt".format(prefix_path, space,  clusters )
    if os.path.isfile(palette_filename):
        print("Found palette on disk {}".format(palette_filename))
        palette = read_palette(palette_filename)
        print("Read palette")
        print(palette)
        if clusters == palette.shape[0]:
            return palette

    print("Coudn't find valid palette on disk. Creating from {}".format(palette_image))

    image = cv2.imread(palette_image)
    image = cv2.cvtColor(image, SPACE_MAP[space]["from_bgr"])

    origX = image.shape[0] 
    origY = image.shape[1] 
    image = image.reshape((origX * origY, 3))
    print("Creating clusters..")
    km = KMeans(n_clusters = clusters)
    km.fit(image)
    palette = km.cluster_centers_
    write_palette(palette, palette_filename, space)
    return palette


def kmeans(image, clusters):
    origX = image.shape[0] 
    origY = image.shape[1] 
    image = image.reshape((origX * origY, 3))
    print("Creating clusters..")
    km = KMeans(n_clusters = clusters)
    km.fit(image)
    palette = km.cluster_centers_
    labels = km.labels_
    for i in range(labels.size):
        index = labels.item(i)
        image.itemset((i,0),palette.item(index,0))
        image.itemset((i,1),palette.item(index,1))
        image.itemset((i,2),palette.item(index,2))

    image = image.reshape((origX , origY, 3))
    return image, palette, labels



def write_palette(palette, filename , space):
    with open(filename, "w") as fobj:
        for value in palette:
            np.savetxt(fobj, value)

    hist = centroid_histogram(np.arange(palette.shape[0]))
    bar =  plot_colors(hist, palette)
    
    bar = cv2.cvtColor(bar,  SPACE_MAP[space]["to_bgr"])
    bar_flename = "{}.png".format(os.path.splitext(filename)[0]) 
    cv2.imwrite( bar_flename, bar)

def read_palette(filename):
 
    return np.loadtxt(filename).reshape(-1, 3)


def set_to_closest(image, palette):
    origX = image.shape[0] 
    origY = image.shape[1] 
    num_pixels = origX * origY
    image = image.reshape((num_pixels, 3))

    labels = np.zeros(num_pixels, np.uint8)

    for i in range(num_pixels):
        index = closest(image[i],palette)[0]
        # print("INDEX", index)
        labels.itemset((i), index)
        image.itemset((i,0),palette.item(index,0))
        image.itemset((i,1),palette.item(index,1))
        image.itemset((i,2),palette.item(index,2))
        # raise ValueError("INTERRUPTED")

    image = image.reshape((origX , origY, 3))
    return image, labels 


def closest(color, palette):

    distances = np.sum((palette-color)**2,axis=1)
    # distances =  [np.sum((p-color)**2) for p in palette]
    # print("DDD:",distances)


    index = (np.where(distances==np.amin(distances)))[0]

    # closest = palette[index]

    return index 


