
# import the necessary packages
# from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
import argparse
import utils
import cv2
import sys
import os



def main():

    ap = argparse.ArgumentParser() 

    ap.add_argument('image', help = "Path to the image")

    ap.add_argument("-p", "--palette_image", help = "Palette image: Will use cached palette if available <basenamex>_<clusters>_<space>.txt")

    ap.add_argument("-o", "--outdir", default=".", help = "Output folder: defaults to current folder")

    ap.add_argument("-m", "--median", type = int,
        help = "Use median filter with kernel size in pixels. It should be a positive odd number")

    # cv2.bilateralFilter(img,9,75,75)
    ap.add_argument("-b", "--bilateral",  nargs=3, type = int,
        help = "Bilateral size - 3 numbers are (size, sigmaColor, sigmaSpace) - see help https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html#bilateralfilter")

    ap.add_argument("-c", "--clusters",   type = int, default=8,
        help = "# of clusters (default: %(default)s)")

    ap.add_argument("-s", "--space",  choices=['lab', 'yrb', 'hsv', 'rgb'], default="yrb", help = "Color Space for distance computation.  (default: %(default)s)")

    args = vars(ap.parse_args())
 
    if args["palette_image"]:
        create_from_custom_palette(args )
    else:
        create_from_image(args )

def create_from_custom_palette(args):
    space = args["space"]
    clusters = args["clusters"]
    palette_image = args["palette_image"]

    palette = utils.get_palette(palette_image, space,  clusters )
    input_image = args["image"]

    image = cv2.imread(input_image)
    image = utils.filter_image(image, args)
    image = cv2.cvtColor(image, utils.SPACE_MAP[space]["from_bgr"])

    # image is now in correct space and we have a palette in that space.
    image, labels = utils.set_to_closest(image, palette)
    hist = utils.centroid_histogram(labels)
    bar = utils.plot_colors(hist, palette)

    image = cv2.cvtColor(image, utils.SPACE_MAP[space]["to_bgr"])
    bar = cv2.cvtColor(bar,  utils.SPACE_MAP[space]["to_bgr"])

    write_images(args, bar=bar,main=image)


def create_from_image(args):
    space = args["space"]
    clusters = args["clusters"]
    input_image = args["image"]

    image = cv2.imread(input_image)
    image = utils.filter_image(image, args)
    image = cv2.cvtColor(image, utils.SPACE_MAP[space]["from_bgr"])

    image, palette, labels = utils.kmeans(image, clusters)
    hist = utils.centroid_histogram(labels)
    bar = utils.plot_colors(hist, palette)

    image = cv2.cvtColor(image, utils.SPACE_MAP[space]["to_bgr"])
    bar = cv2.cvtColor(bar,  utils.SPACE_MAP[space]["to_bgr"])


    write_images(args, bar=bar,main=image)


def write_images(args, **kw):
    space = args["space"]
    clusters = args["clusters"]

    src = os.path.splitext(os.path.basename(args["palette_image"]))[0] if args["palette_image"]  else "self"


    basename = os.path.splitext(os.path.basename(args["image"]))[0]
    outdir = os.path.abspath(args["outdir"])
    utils.ensure_directory(outdir)

    for k in kw:
        path = os.path.join(outdir, "{}_{}_{}_{}_{}.png".format(basename,src,space,clusters,k)) 
        print("Writing image: {}".format(path))
        cv2.imwrite( path, kw[k])




main()

print("Done!")
