
# import the necessary packages
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
import argparse
import utils
import cv2
import sys
import os

ap = argparse.ArgumentParser()

args = vars(ap.parse_args())

out_img_path = "/Users/julian/dev/col/kmeans-clustering/images/all512.png"
start = 2
step = 4
full = 256
res = 512

x=0
y=0

image = np.zeros((res,res,3), np.uint8)

for r in range(start, full, step):
    for g in range(start, full, step):
        for b in range(start, full, step):
            image[x,y] = [b,r,g]
            x+=1
            if x == 512:
                x=0
                y+=1

cv2.imwrite( out_img_path, image )

print("done!")




